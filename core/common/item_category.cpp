//
// Created by Pavel Kasila on 8.05.24.
//

#include "item_category.h"

#include <QSqlRecord>
#include <QString>

namespace outfit::core::common {
ItemCategory::ItemCategory(const QSqlRecord& record) {
    if (record.indexOf("id") != -1) {
        setID(record.field("id").value().toInt());
    }
    if (record.indexOf("name") != -1) {
        setName(record.field("name").value().toString());
    }
}

QString ItemCategory::getName() {
    return name_;
}

void ItemCategory::setName(const QString& name) {
    name_ = name;
}
}  // namespace outfit::core::common
