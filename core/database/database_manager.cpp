//
// Created by Pavel Kasila on 24.04.24.
//

#include "core/database/database_manager.h"

#include <QDir>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QString>
#include <memory>

namespace outfit::core::database {
DatabaseManager::DatabaseManager() {
    const QString data = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir().mkpath(data);
    path_ = data + "/app.db";
}

bool DatabaseManager::connect() {
    database_ = std::make_unique<QSqlDatabase>(QSqlDatabase::addDatabase("QSQLITE"));
    database_->setDatabaseName(path_);
    database_->open();

    if (!database_->isOpen()) {
        return false;
    }

    if (!applyMigrations()) {
        return false;
    }

    return true;
}

bool DatabaseManager::applyMigrations() {
    if (!migrationCreateTables()) {
        return false;
    }

    if (!migrationAddItemCategories()) {
        return false;
    }

    if (!migrationAddItemMaterials()) {
        return false;
    }

    if (!migrationAddItemSizes()) {
        return false;
    }

    if (!migrationAddSeasons()) {
        return false;
    }

    return true;
}

bool DatabaseManager::migrationCreateTables() {
    QSqlQuery query;
    if (!query.exec("create table if not exists item_categories "
                    "(id integer primary key, "
                    "name varchar(50))")) {
        return false;
    }
    if (!query.exec("create table if not exists item_materials "
                    "(id integer primary key, "
                    "name varchar(50))")) {
        return false;
    }
    if (!query.exec("create table if not exists item_sizes "
                    "(id integer primary key, "
                    "name varchar(50))")) {
        return false;
    }
    if (!query.exec("create table if not exists seasons "
                    "(id integer primary key, "
                    "name varchar(50))")) {
        return false;
    }
    if (!query.exec(
            "create table if not exists items("
            "id integer primary key, "
            "category_id integer, "
            "material_id integer, "
            "size_id integer, "
            "brand varchar(50),"
            "name varchar(50),"
            "details text,"
            "style varchar(50),"
            "pattern varchar(50),"
            "wash_principle varchar(50),"
            "season_id integer, "
            "cost float,"
            "foreign key(category_id) REFERENCES item_categories(id),"
            "foreign key(material_id) REFERENCES item_materials(id),"
            "foreign key(size_id) REFERENCES item_sizes(id),"
            "foreign key(season_id) REFERENCES seasons(id)"
            ")")) {
        return false;
    }

    return true;
}

bool DatabaseManager::migrationAddItemCategories() {
    QSqlQuery query;

    return query.exec(
        "insert or ignore into item_categories (id, name) values "
        "(1, 'Blazer'),"
        "(2, 'Scarf'),"
        "(3, 'Watch'),"
        "(4, 'Necklace'),"
        "(5, 'Sweater'),"
        "(6, 'Jeans'),"
        "(7, 'Hat'),"
        "(8, 'Vest'),"
        "(9, 'Bag'),"
        "(10, 'Glasses'),"
        "(11, 'Earrings'),"
        "(12, 'Dress')");
}

bool DatabaseManager::migrationAddItemMaterials() {
    QSqlQuery query;

    return query.exec(
        "insert or ignore into item_materials (id, name) values "
        "(1, 'Linen'),"
        "(2, 'Cashmere'),"
        "(3, 'Cotton'),"
        "(4, 'Cellulosic fibres/viscose'),"
        "(5, 'Wool'),"
        "(6, 'Hemp'),"
        "(7, 'Denim'),"
        "(8, 'Leather'),"
        "(9, 'Fur'),"
        "(10, 'Nylon'),"
        "(11, 'Polyesters'),"
        "(12, 'Spandex')");
}

bool DatabaseManager::migrationAddItemSizes() {
    QSqlQuery query;

    return query.exec(
        "insert or ignore into item_sizes (id, name) values "
        "(1, 'XS'),"
        "(2, 'S'),"
        "(3, 'M'),"
        "(4, 'L'),"
        "(5, 'XL'),"
        "(6, 'XXL'),"
        "(7, 'XXXL')");
}

bool DatabaseManager::migrationAddSeasons() {
    QSqlQuery query;

    return query.exec(
        "insert or ignore into seasons (id, name) values "
        "(1, 'Winter'),"
        "(2, 'Spring'),"
        "(3, 'Summer'),"
        "(4, 'Autumn')");
}

DatabaseManager::~DatabaseManager() {
    database_->close();
}
}  // namespace outfit::core::database