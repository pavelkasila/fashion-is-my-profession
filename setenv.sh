alias 'rcc'='bazel run @hedron_compile_commands//:refresh_all'
alias 'build'='bazel build //cpp/...'
alias 'test'='bazel test //cpp/...'
alias 'cf'='./tools/run_clang_tools.sh'

echo "* Refresh compile commands - rcc"
echo "* Run bazel build for the cpp project - build"
echo "* Run bazel test for the cpp project - test"
echo "* Run clang-format on all cpp and h files - cf"
